import { createRouter, createWebHistory } from 'vue-router';

import Home from '@/views/Home.vue';
import about from '@/views/about.vue';


const routes = [
	{
		path: '/',
		name: 'Home',
		component: Home,
	},
    {
		path: '/rated',
		name: 'about',
		component: about,
	},
];

const router = createRouter({
	history: createWebHistory(),
	routes,
});

export default router;